from .views import RegisterAPI, LoginAPI
from knox import views as knox_views
# from django.urls import url
from django.conf.urls import url

urlpatterns = [
    url('api/register/', RegisterAPI.as_view(), name='register'),
    url('api/login/', LoginAPI.as_view(), name='login'),
    url('api/logout/', knox_views.LogoutView.as_view(), name='logout'),
    url('api/logoutall/', knox_views.LogoutAllView.as_view(), name='logoutall'),
]